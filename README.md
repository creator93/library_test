This is a test project to teach myself how to link a library to a c file. 

(*All files need to be in the same folder.*)

1. First compile the library file "thresholds.h" with:
	- clang -o thresholds.o -c thresholds.c
	to create the object code of the library.
2. Link the library to the main.c file when compiling with:
	- clang main.c -o test_out thresholds.o
3. Run the executable and enjoy!

(*The implementation was tested with clang, but will probably be very similar with gcc and other compilers*)
