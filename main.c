#include <stdio.h>
#include "thresholds.h"

/* This project was aimed to teach myself how to link a new, custom
library file to a .c file. In this mockup, I read 3 values representing
voltage, current and temperature, I compare with internal library thresholds( nice ;) ) 
and print warnings and error messages depending on the situation.*/

int main(){

    read_data(); //reads data from the stdin
    check_and_raise_flags(); //checks internal flags
    print_data(); //prints out the condition of the system
    
}