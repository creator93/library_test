#ifndef _THRESHOLDS_
#define _THRESHOLDS_

#include <stdbool.h>

extern bool input_voltage_warning_flag, input_voltage_error_flag;
extern bool input_current_warning_flag, input_current_error_flag;
extern bool system_temperature_warning_flag, system_temperature_error_flag;
extern bool output_voltage_warning_flag, output_voltage_error_flag;
extern bool output_current_warning_flag, output_current_error_flag;
extern bool se_temperature_warning_flag, se_temperature_error_flag;

//extern struct systemParameters system_parameters;

extern void check_and_raise_flags();
extern void read_data();
extern void print_data();

#endif