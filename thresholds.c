#include <stdio.h>
#include <stdbool.h>
#include "thresholds.h"

#define INPUT_VOLTAGE_WARNING_THRESHOLD 100
#define INPUT_VOLTAGE_ERROR_THRESHOLD 150
#define INPUT_CURRENT_WARNING_THRESHOLD 5
#define INPUT_CURRENT_ERROR_THRESHOLD 9
#define SYSTEM_TEMPERATURE_WARNING_THRESHOLD  50
#define SYSTEM_TEMPERATURE_ERROR_THRESHOLD 75
#define OUTPUT_VOLTAGE_WARNING_THRESHOLD 200
#define OUTPUT_VOLTAGE_ERROR_THRESHOLD 250
#define OUTPUT_CURRENT_WARNING_THRESHOLD 3
#define OUTPUT_CURRENT_ERROR_THRESHOLD 5
#define SE_TEMPERATURE_WARNING_THRESHOLD 25
#define SE_TEMPERATURE_ERROR_THRESHOLD 35

typedef struct sysParams
{
    int input_voltage;
    int input_current;
    int output_voltage;
    int output_current;
    int system_temperature;
    int se_temperature;
} systemParameter;

//int input_voltage, input_current, system_temperature;
//int output_voltage, output_current, se_tempereature;

// flag initialization
bool input_voltage_warning_flag = false;
bool input_voltage_error_flag = false;
bool input_current_warning_flag = false;
bool input_current_error_flag = false;
bool system_temperature_warning_flag = false;
bool system_temperature_error_flag = false;
bool output_voltage_warning_flag = false;
bool output_voltage_error_flag = false;
bool output_current_warning_flag = false;
bool output_current_error_flag = false;
bool se_temperature_warning_flag = false;
bool se_temperature_error_flag = false;

systemParameter system_parameters;
systemParameter * sparamPointer = &system_parameters;

void read_data(){

    printf("Please input system parameters.\n");
    printf("Input voltage = ");
    scanf("%d", &sparamPointer -> input_voltage);
    printf("Input current = ");
    scanf("%d", &sparamPointer -> input_current);
    printf("System temperature = ");
    scanf("%d", &sparamPointer -> system_temperature);
    printf("Output voltage = ");
    scanf("%d", &sparamPointer -> output_voltage);
    printf("Output current = ");
    scanf("%d", &sparamPointer -> output_current);
    printf("SE temperature = ");
    scanf("%d", &sparamPointer -> se_temperature);
    printf("\n");

}

void check_and_raise_flags(){

    // raises appropriate flags for warnings and errors (threshold exclusive)
    if (system_parameters.input_voltage > INPUT_VOLTAGE_WARNING_THRESHOLD){
        input_voltage_warning_flag = true;
    }

    if (system_parameters.input_voltage > INPUT_VOLTAGE_ERROR_THRESHOLD){
        input_voltage_error_flag = true;
    }

    if (system_parameters.input_current > INPUT_CURRENT_WARNING_THRESHOLD){
        input_current_warning_flag = true;
    }

    if (system_parameters.input_current > INPUT_CURRENT_ERROR_THRESHOLD){
        input_current_error_flag = true;
    }

    if (system_parameters.system_temperature > SYSTEM_TEMPERATURE_WARNING_THRESHOLD){
        system_temperature_warning_flag = true;
    }

    if (system_parameters.system_temperature > SYSTEM_TEMPERATURE_ERROR_THRESHOLD){
        system_temperature_error_flag = true;
    }

    if (system_parameters.output_voltage > OUTPUT_VOLTAGE_WARNING_THRESHOLD){
        output_voltage_warning_flag = true;
    }

    if (system_parameters.output_voltage > OUTPUT_VOLTAGE_ERROR_THRESHOLD){
        output_voltage_error_flag = true;
    }

    if (system_parameters.output_current > OUTPUT_CURRENT_WARNING_THRESHOLD){
        output_current_warning_flag = true;
    }

    if (system_parameters.output_current > OUTPUT_CURRENT_ERROR_THRESHOLD){
        output_current_error_flag = true;
    }

    if (system_parameters.se_temperature > SE_TEMPERATURE_WARNING_THRESHOLD){
        se_temperature_warning_flag = true;
    }

    if (system_parameters.se_temperature > SE_TEMPERATURE_ERROR_THRESHOLD){
        se_temperature_error_flag = true;
    }

}

void print_data(){

    // displays appropriate message for input voltage, current and system temperature
    if (input_voltage_error_flag){
        printf("Input voltage exceeds error ehreshold. ERROR!!!\n");
    }
    else if (input_voltage_warning_flag){
        printf("Warning! Large input voltage!\n");
    }
    else{
        printf("Input voltage OK!\n");
    }

    if (input_current_error_flag){
        printf("Input current exceeds error ehreshold. ERROR!!!\n");
    }
    else if (input_current_warning_flag){
        printf("Warning! Large input current!\n");
    }
    else{
        printf("Input current OK!\n");
    }

    if (system_temperature_error_flag){
        printf("System temperature exceeds error ehreshold. ERROR!!!\n");
    }
    else if (system_temperature_warning_flag){
        printf("Warning! Large system temperature!\n");
    }
    else{
        printf("System_temperature OK!\n");
    }

    // displays appropriate message for output voltage, current and se temperature
    if (output_voltage_error_flag){
        printf("Output voltage exceeds error ehreshold. ERROR!!!\n");
    }
    else if (output_voltage_warning_flag){
        printf("Warning! Large output voltage!\n");
    }
    else{
        printf("Output Voltage OK!\n");
    }

    if (output_current_error_flag){
        printf("Output current exceeds error ehreshold. ERROR!!!\n");
    }
    else if (output_current_warning_flag){
        printf("Warning! Large output current!\n");
    }
    else{
        printf("Output current OK!\n");
    }

    if (se_temperature_error_flag){
        printf("SE temperature exceeds error ehreshold. ERROR!!!\n");
    }
    else if (se_temperature_warning_flag){
        printf("Warning! Large SE temperature!\n");
    }
    else{
        printf("SE temperature OK!\n");
    }

    if (!input_voltage_warning_flag && !input_current_warning_flag && !system_temperature_warning_flag){
        printf("All system inputs are clear!\n");
    }

    if (!output_voltage_warning_flag && !output_current_warning_flag && !se_temperature_warning_flag){
        printf("All system outputs are clear!\n");
    }

}